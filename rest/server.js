const express  = require('express');
const bodyParser = require('body-parser');
const dotenv = require('dotenv');
const cors = require('cors');
const postsController = require('./controllers/postsController');

const app = express();

// Load env config
dotenv.config({ path: './config.env' });
app.use(cors());

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use('/api/posts', postsController);

app.listen(process.env.PORT || 3000, () => {
  console.clear();
  console.log(`Server is running on port ${process.env.PORT}`);
});