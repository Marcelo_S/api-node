// posts controller routes
const express = require('express');
const axios = require('axios');
const router = express.Router();

// get /api/posts/
router.get('/', async (req,res) => {
  // Get all posts from API  
  try {
    const {data} = await axios.get(`${process.env.ENDPOINT}/posts`);      
    return res.status(200).send(data);
  } 
  
  catch (error) {    
    return res.status(500).send({error});
  }
});

router.get('/:id', async (req, res) => {  
  const id = req.params.id;
  try {
    const {data} = await axios.get(`${process.env.ENDPOINT}/posts/${id}`);
    return res.status(200).send(data);
  } 
  catch ({message}) {
    return res.status(500).send({error: message});
  }
});

// post /api/posts/
router.post('/', async (req,res) => {
  const {title, body, userId} = req.body;  

  if(!title || !body || userId <= 0){
    return res.status(500).send({error: "All fields are required"});    
  }

  const config = {
    headers: {
      "Content-type": "application/json; charset=UTF-8"
    }
  }
        
  const {status} = await axios.post(`${process.env.ENDPOINT}/posts`, { title, body, userId }, config);

  if(status === 200 || status === 201){
      return res.status(200).send({message: "Post saved"});    
  } else {
      return res.status(500).send({error: "There has been an error, please try again"});    
  }

  // axios.post(`${process.env.ENDPOINT}/posts`, { title, body, userId }, config)
  // .then(response => {    
  //     const {data, status} = response;      
  //     return res.status(200).send(data);    
  //   })
  //   .catch(error => {
  //     console.log(error);
  //     return res.status(500).send({error});    
  // })
});



// post /api/posts/
router.post('/:id', async (req,res) => {
  const {id} = req.params;
  const {title, body, userId} = req.body;

  if(!title || !body || userId <= 0){
    return res.status(400).send({ error : "Error all fields are required" });
  }

  const header = {
    headers: {
      "Content-type": "application/json; charset=UTF-8"
    }
  }

  try {
    const {status, data} = await axios.put(`${process.env.ENDPOINT}/posts/${id}`, { id, title, body, userId }, header);
    
    if ( status === 200  || status === 201){
      return res.status(200).send(data);
    } else {
      return res.status(500).send({ error : "There has been an error"});
    }  
  }
  catch ({message}) {
    return res.status(500).send({ message });
  }

});

module.exports = router;